import mongoose from 'mongoose';
import bcrypt from 'bcrypt'
const { Schema } = mongoose;

const User = new Schema({
  email:{
    type:String,
    minlength:[3,"email minimum 3 charachter"],
    maxLength:[255,"email maximum 255 charachter"],
    required:[true,"email is required"]
  },
  name:{
    type:String,
    minlength:[3,"email minimum 3 charachter"],
    maxLength:[255,"email maximum 255 charachter"],
    required:[true,"email is required"]
  },
  password:{
    type:String,
    minlength:[3,"email minimum 3 charachter"],
    maxLength:[255,"email maximum 255 charachter"],
    required:[true,"email is required"]
  }
},{timestamp:true});
const HASH_ROUND = 10;
User.pre("save",function (next) {
    this.password = bcrypt.hashSync(this.password, HASH_ROUND);
    next();
})
export default mongoose.model('User',User)